import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { environment } from "../../environments/environment";
import { Game } from '../interfaces/interfaces';
import { catchError, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private juegos: Game[] = [];

  constructor(private http: HttpClient) { }

  getNominados() {
    if ( this.juegos.length === 0 ){
      // no existen juegos
      console.log('desde api');
      return this.http.get<Game[]>(`${environment.url}/api/goty`)
      .pipe(tap(
        juegos => this.juegos = juegos
      ) );
    } else {
      console.log('desde memoria');
      return of(this.juegos);
    }

  }

  votarJuego(id: string) {
    return this.http.post(`${environment.url}/api/goty/${ id }`, {})
    .pipe(
      catchError( err => {
        // console.log('error en la peticion');
        return of( err.error );
      })
    );
  }
}
