import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single } from '../../interfaces/data';

@Component({
  selector: 'app-grafico-barra-horizontal',
  templateUrl: './grafico-barra-horizontal.component.html',
  styleUrls: ['./grafico-barra-horizontal.component.css']
})
export class GraficoBarraHorizontalComponent implements OnInit, OnDestroy {

  @Input() result: any[] = [];

  // result: any[] = [
  //   {
  //     "name": "Juego 1",
  //     "value": 20
  //   },
  //   {
  //     "name": "Juego 2",
  //     "value": 25
  //   },
  //   {
  //     "name": "Juego 3",
  //     "value": 15
  //   },
  //   {
  //     "name": "Juego 4",
  //     "value": 30
  //   }

  // ];

  // multi: any[];

  // view: [number, number] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Juegos';
  showYAxisLabel = true;
  yAxisLabel = 'Votos';

  colorScheme = 'nightLights';

  // intervalo;

  constructor() {
    // Object.assign(this, { single });


    // this.intervalo = setInterval( () => {
    //   const newResult = [...this.result];
    //   for (const i in newResult) {
    //     newResult[i].value = Math.round( Math.random() * 500) ;
    //   }

    //   this.result = [...newResult];
    // }, 1500);
  }

  ngOnInit(): void {
  }

  onSelect(event) {
    console.log(event);
  }

  ngOnDestroy() {
// clearInterval(this.intervalo);
  }

}
